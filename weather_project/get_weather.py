# -*- coding: utf-8 -*-
import sys
import json
from geopy.geocoders import Nominatim
import urllib2
import datetime
import time

class GetWeather(object):
    ''' This object will get weather information '''

    def __init__(self):
    
        self.data_time = "daily"
        self.parsed_json_weather = ""
        self.test = []
        # Test Json String
        self.test_json_weather_string = '{"latitude":51.4816546,"longitude":-3.1791933,"timezone":"Europe/London","offset":1,"currently":{"time":1476283388,"summary":"Partly Cloudy","icon":"partly-cloudy-day","nearestStormDistance":0,"precipIntensity":0.0052,"precipIntensityError":0.0017,"precipProbability":0.27,"precipType":"rain","temperature":58.66,"apparentTemperature":58.66,"dewPoint":49.45,"humidity":0.71,"windSpeed":12.51,"windBearing":0,"visibility":8.18,"cloudCover":0.52,"pressure":1019.05,"ozone":278.95},"minutely":{"summary":"Partly cloudy for the hour.","icon":"partly-cloudy-day","data":[{"time":1476283380,"precipIntensity":0.0053,"precipIntensityError":0.0016,"precipProbability":0.29,"precipType":"rain"},{"time":1476283440,"precipIntensity":0.005,"precipIntensityError":0.0018,"precipProbability":0.19,"precipType":"rain"},{"time":1476283500,"precipIntensity":0.0047,"precipIntensityError":0.0017,"precipProbability":0.11,"precipType":"rain"},{"time":1476283560,"precipIntensity":0.0043,"precipIntensityError":0.0018,"precipProbability":0.05,"precipType":"rain"},{"time":1476283620,"precipIntensity":0.0039,"precipIntensityError":0.0017,"precipProbability":0.02,"precipType":"rain"},{"time":1476283680,"precipIntensity":0.0037,"precipIntensityError":0.0017,"precipProbability":0.01,"precipType":"rain"},{"time":1476283740,"precipIntensity":0.0041,"precipIntensityError":0.002,"precipProbability":0.01,"precipType":"rain"},{"time":1476283800,"precipIntensity":0.0052,"precipIntensityError":0.0023,"precipProbability":0.04,"precipType":"rain"},{"time":1476283860,"precipIntensity":0.0054,"precipIntensityError":0.0024,"precipProbability":0.06,"precipType":"rain"},{"time":1476283920,"precipIntensity":0.0056,"precipIntensityError":0.0024,"precipProbability":0.1,"precipType":"rain"},{"time":1476283980,"precipIntensity":0.0057,"precipIntensityError":0.0024,"precipProbability":0.15,"precipType":"rain"},{"time":1476284040,"precipIntensity":0.0057,"precipIntensityError":0.0024,"precipProbability":0.16,"precipType":"rain"},{"time":1476284100,"precipIntensity":0.0056,"precipIntensityError":0.0024,"precipProbability":0.15,"precipType":"rain"},{"time":1476284160,"precipIntensity":0.0055,"precipIntensityError":0.0023,"precipProbability":0.13,"precipType":"rain"},{"time":1476284220,"precipIntensity":0.0053,"precipIntensityError":0.0023,"precipProbability":0.09,"precipType":"rain"},{"time":1476284280,"precipIntensity":0.0052,"precipIntensityError":0.0024,"precipProbability":0.06,"precipType":"rain"},{"time":1476284340,"precipIntensity":0.0051,"precipIntensityError":0.0023,"precipProbability":0.04,"precipType":"rain"},{"time":1476284400,"precipIntensity":0.0051,"precipIntensityError":0.0022,"precipProbability":0.03,"precipType":"rain"},{"time":1476284460,"precipIntensity":0.0049,"precipIntensityError":0.0022,"precipProbability":0.04,"precipType":"rain"},{"time":1476284520,"precipIntensity":0.0047,"precipIntensityError":0.0022,"precipProbability":0.02,"precipType":"rain"},{"time":1476284580,"precipIntensity":0.0047,"precipIntensityError":0.0021,"precipProbability":0.01,"precipType":"rain"},{"time":1476284640,"precipIntensity":0.0046,"precipIntensityError":0.0021,"precipProbability":0.01,"precipType":"rain"},{"time":1476284700,"precipIntensity":0,"precipProbability":0},{"time":1476284760,"precipIntensity":0,"precipProbability":0},{"time":1476284820,"precipIntensity":0,"precipProbability":0},{"time":1476284880,"precipIntensity":0,"precipProbability":0},{"time":1476284940,"precipIntensity":0,"precipProbability":0},{"time":1476285000,"precipIntensity":0,"precipProbability":0},{"time":1476285060,"precipIntensity":0,"precipProbability":0},{"time":1476285120,"precipIntensity":0,"precipProbability":0},{"time":1476285180,"precipIntensity":0,"precipProbability":0},{"time":1476285240,"precipIntensity":0,"precipProbability":0},{"time":1476285300,"precipIntensity":0,"precipProbability":0},{"time":1476285360,"precipIntensity":0,"precipProbability":0},{"time":1476285420,"precipIntensity":0,"precipProbability":0},{"time":1476285480,"precipIntensity":0,"precipProbability":0},{"time":1476285540,"precipIntensity":0,"precipProbability":0},{"time":1476285600,"precipIntensity":0,"precipProbability":0},{"time":1476285660,"precipIntensity":0,"precipProbability":0},{"time":1476285720,"precipIntensity":0,"precipProbability":0},{"time":1476285780,"precipIntensity":0,"precipProbability":0},{"time":1476285840,"precipIntensity":0,"precipProbability":0},{"time":1476285900,"precipIntensity":0,"precipProbability":0},{"time":1476285960,"precipIntensity":0,"precipProbability":0},{"time":1476286020,"precipIntensity":0,"precipProbability":0},{"time":1476286080,"precipIntensity":0,"precipProbability":0},{"time":1476286140,"precipIntensity":0,"precipProbability":0},{"time":1476286200,"precipIntensity":0,"precipProbability":0},{"time":1476286260,"precipIntensity":0,"precipProbability":0},{"time":1476286320,"precipIntensity":0,"precipProbability":0},{"time":1476286380,"precipIntensity":0.0051,"precipIntensityError":0.0023,"precipProbability":0.01,"precipType":"rain"},{"time":1476286440,"precipIntensity":0.0051,"precipIntensityError":0.0023,"precipProbability":0.01,"precipType":"rain"},{"time":1476286500,"precipIntensity":0.0056,"precipIntensityError":0.0024,"precipProbability":0.02,"precipType":"rain"},{"time":1476286560,"precipIntensity":0.0059,"precipIntensityError":0.0025,"precipProbability":0.02,"precipType":"rain"},{"time":1476286620,"precipIntensity":0.0059,"precipIntensityError":0.0025,"precipProbability":0.03,"precipType":"rain"},{"time":1476286680,"precipIntensity":0.0058,"precipIntensityError":0.0026,"precipProbability":0.03,"precipType":"rain"},{"time":1476286740,"precipIntensity":0.0059,"precipIntensityError":0.0026,"precipProbability":0.04,"precipType":"rain"},{"time":1476286800,"precipIntensity":0.006,"precipIntensityError":0.0026,"precipProbability":0.05,"precipType":"rain"},{"time":1476286860,"precipIntensity":0.006,"precipIntensityError":0.0027,"precipProbability":0.06,"precipType":"rain"},{"time":1476286920,"precipIntensity":0.006,"precipIntensityError":0.0027,"precipProbability":0.07,"precipType":"rain"},{"time":1476286980,"precipIntensity":0.006,"precipIntensityError":0.0027,"precipProbability":0.07,"precipType":"rain"}]},"hourly":{"summary":"Partly cloudy throughout the day.","icon":"partly-cloudy-night","data":[{"time":1476280800,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":58.25,"apparentTemperature":58.25,"dewPoint":49.26,"humidity":0.72,"windSpeed":13.15,"windBearing":1,"visibility":7.13,"cloudCover":0.56,"pressure":1019.33,"ozone":279.42},{"time":1476284400,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0.0036,"precipProbability":0.14,"precipType":"rain","temperature":58.82,"apparentTemperature":58.82,"dewPoint":49.52,"humidity":0.71,"windSpeed":12.26,"windBearing":359,"visibility":8.6,"cloudCover":0.5,"pressure":1018.94,"ozone":278.76},{"time":1476288000,"summary":"Drizzle","icon":"rain","precipIntensity":0.0052,"precipProbability":0.21,"precipType":"rain","temperature":57.96,"apparentTemperature":57.96,"dewPoint":49.33,"humidity":0.73,"windSpeed":12.6,"windBearing":359,"visibility":9.07,"cloudCover":0.47,"pressure":1018.8,"ozone":278.81},{"time":1476291600,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":56.63,"apparentTemperature":56.63,"dewPoint":48.66,"humidity":0.75,"windSpeed":12.93,"windBearing":0,"visibility":9.4,"cloudCover":0.45,"pressure":1018.8,"ozone":279.31},{"time":1476295200,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":55.43,"apparentTemperature":55.43,"dewPoint":48.4,"humidity":0.77,"windSpeed":13.45,"windBearing":2,"visibility":9.48,"cloudCover":0.45,"pressure":1018.81,"ozone":280.01},{"time":1476298800,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":54.77,"apparentTemperature":54.77,"dewPoint":48.51,"humidity":0.79,"windSpeed":14.07,"windBearing":7,"visibility":9.52,"cloudCover":0.39,"pressure":1018.79,"ozone":280.94},{"time":1476302400,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":53.9,"apparentTemperature":53.9,"dewPoint":48.23,"humidity":0.81,"windSpeed":14.56,"windBearing":12,"visibility":10,"cloudCover":0.31,"pressure":1018.76,"ozone":282.08},{"time":1476306000,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":53.35,"apparentTemperature":53.35,"dewPoint":48.13,"humidity":0.82,"windSpeed":14.84,"windBearing":15,"visibility":10,"cloudCover":0.25,"pressure":1018.66,"ozone":283.08},{"time":1476309600,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":52.49,"apparentTemperature":52.49,"dewPoint":47.61,"humidity":0.83,"windSpeed":14.99,"windBearing":16,"visibility":10,"cloudCover":0.17,"pressure":1018.49,"ozone":283.76},{"time":1476313200,"summary":"Clear","icon":"clear-night","precipIntensity":0.003,"precipProbability":0.1,"precipType":"rain","temperature":51.83,"apparentTemperature":51.83,"dewPoint":47.21,"humidity":0.84,"windSpeed":14.47,"windBearing":15,"visibility":10,"cloudCover":0.08,"pressure":1018.26,"ozone":284.31},{"time":1476316800,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":51.13,"apparentTemperature":51.13,"dewPoint":46.77,"humidity":0.85,"windSpeed":13.91,"windBearing":15,"visibility":10,"cloudCover":0.02,"pressure":1017.97,"ozone":285.01},{"time":1476320400,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":50.28,"apparentTemperature":50.28,"dewPoint":46.16,"humidity":0.86,"windSpeed":13.23,"windBearing":13,"visibility":10,"cloudCover":0.06,"pressure":1017.6,"ozone":285.97},{"time":1476324000,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":49.6,"apparentTemperature":44.56,"dewPoint":45.67,"humidity":0.86,"windSpeed":13.28,"windBearing":10,"visibility":10,"cloudCover":0.11,"pressure":1017.18,"ozone":287.07},{"time":1476327600,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":48.86,"apparentTemperature":43.54,"dewPoint":45.07,"humidity":0.87,"windSpeed":13.62,"windBearing":9,"visibility":10,"cloudCover":0.15,"pressure":1016.74,"ozone":288.33},{"time":1476331200,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":48.2,"apparentTemperature":42.71,"dewPoint":44.68,"humidity":0.88,"windSpeed":13.58,"windBearing":7,"visibility":10,"cloudCover":0.15,"pressure":1016.32,"ozone":289.86},{"time":1476334800,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":47.62,"apparentTemperature":41.82,"dewPoint":44.26,"humidity":0.88,"windSpeed":14.14,"windBearing":7,"visibility":10,"cloudCover":0.13,"pressure":1015.92,"ozone":291.54},{"time":1476338400,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":47.17,"apparentTemperature":41.21,"dewPoint":43.94,"humidity":0.88,"windSpeed":14.24,"windBearing":9,"visibility":10,"cloudCover":0.12,"pressure":1015.56,"ozone":293.03},{"time":1476342000,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":48.05,"apparentTemperature":42.38,"dewPoint":44.45,"humidity":0.87,"windSpeed":14.12,"windBearing":10,"visibility":10,"cloudCover":0.12,"pressure":1015.29,"ozone":294.18},{"time":1476345600,"summary":"Clear","icon":"clear-day","precipIntensity":0.001,"precipProbability":0.01,"precipType":"rain","temperature":48.93,"apparentTemperature":43.53,"dewPoint":44.62,"humidity":0.85,"windSpeed":13.97,"windBearing":17,"visibility":10,"cloudCover":0.12,"pressure":1015.06,"ozone":295.14},{"time":1476349200,"summary":"Clear","icon":"clear-day","precipIntensity":0.0015,"precipProbability":0.03,"precipType":"rain","temperature":49.83,"apparentTemperature":44.74,"dewPoint":44.71,"humidity":0.82,"windSpeed":13.75,"windBearing":23,"visibility":10,"cloudCover":0.13,"pressure":1014.77,"ozone":295.95},{"time":1476352800,"summary":"Clear","icon":"clear-day","precipIntensity":0.0014,"precipProbability":0.03,"precipType":"rain","temperature":50.79,"apparentTemperature":50.79,"dewPoint":44.6,"humidity":0.79,"windSpeed":13.37,"windBearing":27,"visibility":10,"cloudCover":0.19,"pressure":1014.37,"ozone":296.63},{"time":1476356400,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0.0008,"precipProbability":0.01,"precipType":"rain","temperature":51.96,"apparentTemperature":51.96,"dewPoint":44.67,"humidity":0.76,"windSpeed":12.89,"windBearing":30,"visibility":10,"cloudCover":0.26,"pressure":1013.9,"ozone":297.15},{"time":1476360000,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":53.59,"apparentTemperature":53.59,"dewPoint":45.06,"humidity":0.73,"windSpeed":12.81,"windBearing":41,"visibility":10,"cloudCover":0.34,"pressure":1013.41,"ozone":297.54},{"time":1476363600,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":54.59,"apparentTemperature":54.59,"dewPoint":45.34,"humidity":0.71,"windSpeed":12.26,"windBearing":43,"visibility":10,"cloudCover":0.37,"pressure":1012.84,"ozone":297.8},{"time":1476367200,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":55.75,"apparentTemperature":55.75,"dewPoint":45.77,"humidity":0.69,"windSpeed":11.7,"windBearing":44,"visibility":10,"cloudCover":0.41,"pressure":1012.25,"ozone":297.91},{"time":1476370800,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":56.52,"apparentTemperature":56.52,"dewPoint":46.03,"humidity":0.68,"windSpeed":11.35,"windBearing":44,"visibility":10,"cloudCover":0.43,"pressure":1011.77,"ozone":297.83},{"time":1476374400,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0.0013,"precipProbability":0.02,"precipType":"rain","temperature":55.56,"apparentTemperature":55.56,"dewPoint":45.73,"humidity":0.69,"windSpeed":11.23,"windBearing":42,"visibility":10,"cloudCover":0.48,"pressure":1011.48,"ozone":297.58},{"time":1476378000,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0.002,"precipProbability":0.05,"precipType":"rain","temperature":54.3,"apparentTemperature":54.3,"dewPoint":45.36,"humidity":0.72,"windSpeed":11.35,"windBearing":39,"visibility":10,"cloudCover":0.53,"pressure":1011.3,"ozone":297.14},{"time":1476381600,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0023,"precipProbability":0.06,"precipType":"rain","temperature":53.03,"apparentTemperature":53.03,"dewPoint":44.94,"humidity":0.74,"windSpeed":11.35,"windBearing":37,"visibility":10,"cloudCover":0.56,"pressure":1011.16,"ozone":296.37},{"time":1476385200,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0019,"precipProbability":0.05,"precipType":"rain","temperature":51.8,"apparentTemperature":51.8,"dewPoint":44.39,"humidity":0.76,"windSpeed":11.22,"windBearing":36,"visibility":10,"cloudCover":0.44,"pressure":1011.07,"ozone":295.17},{"time":1476388800,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.001,"precipProbability":0.01,"precipType":"rain","temperature":50.72,"apparentTemperature":50.72,"dewPoint":43.96,"humidity":0.78,"windSpeed":10.93,"windBearing":36,"visibility":10,"cloudCover":0.3,"pressure":1011,"ozone":293.65},{"time":1476392400,"summary":"Clear","icon":"clear-night","precipIntensity":0,"precipProbability":0,"temperature":49.85,"apparentTemperature":45.66,"dewPoint":43.79,"humidity":0.8,"windSpeed":10.61,"windBearing":36,"visibility":10,"cloudCover":0.21,"pressure":1010.85,"ozone":292.06},{"time":1476396000,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":49.32,"apparentTemperature":45.07,"dewPoint":43.9,"humidity":0.81,"windSpeed":10.38,"windBearing":32,"visibility":10,"cloudCover":0.26,"pressure":1010.53,"ozone":290.7},{"time":1476399600,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":48.86,"apparentTemperature":44.56,"dewPoint":44.11,"humidity":0.84,"windSpeed":10.21,"windBearing":28,"visibility":10,"cloudCover":0.34,"pressure":1010.11,"ozone":289.27},{"time":1476403200,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":48.27,"apparentTemperature":43.88,"dewPoint":44.08,"humidity":0.85,"windSpeed":10.04,"windBearing":24,"visibility":10,"cloudCover":0.39,"pressure":1009.69,"ozone":287.08},{"time":1476406800,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":47.72,"apparentTemperature":43.34,"dewPoint":43.82,"humidity":0.86,"windSpeed":9.63,"windBearing":23,"visibility":10,"cloudCover":0.46,"pressure":1009.28,"ozone":283.33},{"time":1476410400,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":47.12,"apparentTemperature":42.82,"dewPoint":43.43,"humidity":0.87,"windSpeed":9.08,"windBearing":23,"visibility":10,"cloudCover":0.51,"pressure":1008.87,"ozone":278.83},{"time":1476414000,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":46.58,"apparentTemperature":42.36,"dewPoint":43.1,"humidity":0.88,"windSpeed":8.56,"windBearing":23,"visibility":9.55,"cloudCover":0.55,"pressure":1008.48,"ozone":275.24},{"time":1476417600,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":47.09,"apparentTemperature":43.19,"dewPoint":43.8,"humidity":0.88,"windSpeed":8.09,"windBearing":24,"visibility":9.55,"cloudCover":0.54,"pressure":1008.13,"ozone":273.29},{"time":1476421200,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":47.65,"apparentTemperature":44.04,"dewPoint":44.56,"humidity":0.89,"windSpeed":7.68,"windBearing":26,"visibility":9.55,"cloudCover":0.53,"pressure":1007.81,"ozone":272.26},{"time":1476424800,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":48.15,"apparentTemperature":44.82,"dewPoint":45.21,"humidity":0.9,"windSpeed":7.3,"windBearing":29,"visibility":9.55,"cloudCover":0.53,"pressure":1007.55,"ozone":271.65},{"time":1476428400,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":48.88,"apparentTemperature":45.83,"dewPoint":45.9,"humidity":0.89,"windSpeed":7.04,"windBearing":32,"visibility":9.55,"cloudCover":0.56,"pressure":1007.4,"ozone":271.46},{"time":1476432000,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":49.5,"apparentTemperature":46.66,"dewPoint":46.41,"humidity":0.89,"windSpeed":6.85,"windBearing":37,"visibility":9.55,"cloudCover":0.62,"pressure":1007.31,"ozone":271.7},{"time":1476435600,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":50.12,"apparentTemperature":50.12,"dewPoint":46.77,"humidity":0.88,"windSpeed":6.63,"windBearing":43,"visibility":9.55,"cloudCover":0.64,"pressure":1007.19,"ozone":271.87},{"time":1476439200,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":51.27,"apparentTemperature":51.27,"dewPoint":47.23,"humidity":0.86,"windSpeed":6.18,"windBearing":52,"visibility":9.55,"cloudCover":0.63,"pressure":1007,"ozone":271.84},{"time":1476442800,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":52.61,"apparentTemperature":52.61,"dewPoint":47.69,"humidity":0.83,"windSpeed":5.77,"windBearing":63,"visibility":9.55,"cloudCover":0.59,"pressure":1006.78,"ozone":271.74},{"time":1476446400,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":54.23,"apparentTemperature":54.23,"dewPoint":48.44,"humidity":0.81,"windSpeed":5.49,"windBearing":78,"visibility":9.55,"cloudCover":0.56,"pressure":1006.52,"ozone":271.52},{"time":1476450000,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":55.74,"apparentTemperature":55.74,"dewPoint":49.37,"humidity":0.79,"windSpeed":5.36,"windBearing":83,"visibility":9.55,"cloudCover":0.54,"pressure":1006.18,"ozone":271.19},{"time":1476453600,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":57.26,"apparentTemperature":57.26,"dewPoint":50.36,"humidity":0.78,"windSpeed":5.2,"windBearing":86,"visibility":9.55,"cloudCover":0.52,"pressure":1005.81,"ozone":270.74}]},"daily":{"summary":"Light rain today through Tuesday, with temperatures peaking at 62┬░F on Sunday.","icon":"rain","data":[{"time":1476226800,"summary":"Partly cloudy throughout the day.","icon":"partly-cloudy-day","sunriseTime":1476253991,"sunsetTime":1476293238,"moonPhase":0.36,"precipIntensity":0.0006,"precipIntensityMax":0.0052,"precipIntensityMaxTime":1476288000,"precipProbability":0.21,"precipType":"rain","temperatureMin":50.76,"temperatureMinTime":1476241200,"temperatureMax":59.13,"temperatureMaxTime":1476273600,"apparentTemperatureMin":50.76,"apparentTemperatureMinTime":1476241200,"apparentTemperatureMax":59.13,"apparentTemperatureMaxTime":1476273600,"dewPoint":48.77,"humidity":0.82,"windSpeed":12.39,"windBearing":8,"visibility":8.01,"cloudCover":0.47,"pressure":1020.13,"ozone":284.32},{"time":1476313200,"summary":"Partly cloudy starting in the afternoon.","icon":"partly-cloudy-night","sunriseTime":1476340492,"sunsetTime":1476379508,"moonPhase":0.4,"precipIntensity":0.0008,"precipIntensityMax":0.003,"precipIntensityMaxTime":1476313200,"precipProbability":0.1,"precipType":"rain","temperatureMin":47.17,"temperatureMinTime":1476338400,"temperatureMax":56.52,"temperatureMaxTime":1476370800,"apparentTemperatureMin":41.21,"apparentTemperatureMinTime":1476338400,"apparentTemperatureMax":56.52,"apparentTemperatureMaxTime":1476370800,"dewPoint":45.05,"humidity":0.8,"windSpeed":12.31,"windBearing":25,"visibility":10,"cloudCover":0.25,"pressure":1014.02,"ozone":293.25},{"time":1476399600,"summary":"Light rain overnight.","icon":"rain","sunriseTime":1476426994,"sunsetTime":1476465778,"moonPhase":0.44,"precipIntensity":0.0008,"precipIntensityMax":0.0023,"precipIntensityMaxTime":1476468000,"precipProbability":0.06,"precipType":"rain","temperatureMin":46.58,"temperatureMinTime":1476414000,"temperatureMax":58.24,"temperatureMaxTime":1476457200,"apparentTemperatureMin":42.36,"apparentTemperatureMinTime":1476414000,"apparentTemperatureMax":58.24,"apparentTemperatureMaxTime":1476457200,"dewPoint":46.83,"humidity":0.84,"windSpeed":6.4,"windBearing":50,"visibility":10,"cloudCover":0.54,"pressure":1006.98,"ozone":272.57},{"time":1476486000,"summary":"Light rain in the morning and afternoon.","icon":"rain","sunriseTime":1476513496,"sunsetTime":1476552049,"moonPhase":0.47,"precipIntensity":0.0061,"precipIntensityMax":0.0201,"precipIntensityMaxTime":1476500400,"precipProbability":0.53,"precipType":"rain","temperatureMin":49.72,"temperatureMinTime":1476500400,"temperatureMax":60.5,"temperatureMaxTime":1476536400,"apparentTemperatureMin":46.72,"apparentTemperatureMinTime":1476500400,"apparentTemperatureMax":60.5,"apparentTemperatureMaxTime":1476536400,"dewPoint":49.43,"humidity":0.83,"windSpeed":6.79,"windBearing":121,"visibility":10,"cloudCover":0.48,"pressure":1006.49,"ozone":271.22},{"time":1476572400,"summary":"Rain until afternoon.","icon":"rain","sunriseTime":1476599998,"sunsetTime":1476638322,"moonPhase":0.51,"precipIntensity":0.0296,"precipIntensityMax":0.119,"precipIntensityMaxTime":1476608400,"precipProbability":0.7,"precipType":"rain","temperatureMin":52.02,"temperatureMinTime":1476655200,"temperatureMax":62.49,"temperatureMaxTime":1476626400,"apparentTemperatureMin":52.02,"apparentTemperatureMinTime":1476655200,"apparentTemperatureMax":62.49,"apparentTemperatureMaxTime":1476626400,"dewPoint":51.96,"humidity":0.84,"windSpeed":13.05,"windBearing":172,"visibility":10,"cloudCover":0.41,"pressure":1011.85,"ozone":276.21},{"time":1476658800,"summary":"Light rain until evening.","icon":"rain","sunriseTime":1476686501,"sunsetTime":1476724595,"moonPhase":0.55,"precipIntensity":0.0069,"precipIntensityMax":0.0124,"precipIntensityMaxTime":1476712800,"precipProbability":0.47,"precipType":"rain","temperatureMin":50.44,"temperatureMinTime":1476669600,"temperatureMax":61.43,"temperatureMaxTime":1476712800,"apparentTemperatureMin":50.44,"apparentTemperatureMinTime":1476669600,"apparentTemperatureMax":61.43,"apparentTemperatureMaxTime":1476712800,"dewPoint":49.76,"humidity":0.82,"windSpeed":5.33,"windBearing":220,"visibility":10,"cloudCover":0.56,"pressure":1015.17,"ozone":295.41},{"time":1476745200,"summary":"Light rain until evening and breezy starting in the afternoon, continuing until evening.","icon":"rain","sunriseTime":1476773004,"sunsetTime":1476810869,"moonPhase":0.59,"precipIntensity":0.0091,"precipIntensityMax":0.0255,"precipIntensityMaxTime":1476802800,"precipProbability":0.55,"precipType":"rain","temperatureMin":50.69,"temperatureMinTime":1476828000,"temperatureMax":58.98,"temperatureMaxTime":1476799200,"apparentTemperatureMin":50.69,"apparentTemperatureMinTime":1476828000,"apparentTemperatureMax":58.98,"apparentTemperatureMaxTime":1476799200,"dewPoint":48.56,"humidity":0.81,"windSpeed":14.66,"windBearing":258,"cloudCover":0.46,"pressure":1016.81,"ozone":297.7},{"time":1476831600,"summary":"Mostly cloudy starting in the afternoon, continuing until evening.","icon":"partly-cloudy-day","sunriseTime":1476859507,"sunsetTime":1476897143,"moonPhase":0.62,"precipIntensity":0.0021,"precipIntensityMax":0.0034,"precipIntensityMaxTime":1476900000,"precipProbability":0.11,"precipType":"rain","temperatureMin":48.38,"temperatureMinTime":1476846000,"temperatureMax":55.69,"temperatureMaxTime":1476885600,"apparentTemperatureMin":42.52,"apparentTemperatureMinTime":1476849600,"apparentTemperatureMax":55.69,"apparentTemperatureMaxTime":1476885600,"dewPoint":43.74,"humidity":0.74,"windSpeed":15.97,"windBearing":289,"cloudCover":0.27,"pressure":1018.79,"ozone":306.41}]},"flags":{"sources":["darksky","datapoint","gfs","cmc","nam","rap","sref","fnmoc","isd","metwarn","madis","nearest-precip"],"darksky-stations":["uk_london"],"datapoint-stations":["uk-350758","uk-350759","uk-350761","uk-350762","uk-350763","uk-351382","uk-351531","uk-351819","uk-352324","uk-352325","uk-352490","uk-352732","uk-353561","uk-353623","uk-353977","uk-371381"],"isd-stations":["036140-99999","037150-99999","037155-99999","037160-99999","037170-99999"],"madis-stations":["EGDX","EGFF","EGGD"],"units":"us"}}'
        
    def get_weather_data(self, darksky_url):
        self.weather_url = darksky_url
        try:
            print log_output().print_output("Getting weather data...")
            self.open_json_weather_data = urllib2.urlopen(self.weather_url)
            self.json_weather_data = self.open_json_weather_data.read()
            self.open_json_weather_data.close()
            
        except urllib2.HTTPError, e:
            checksLogger.error('HTTPError = ' + str(e.code))
        except urllib2.URLError, e:
            checksLogger.error('URLError = ' + str(e.reason))
        except httplib.HTTPException, e:
            checksLogger.error('HTTPException')
        except Exception:
            import traceback
            checksLogger.error('generic exception: ' + traceback.format_exc())
            
        return self.json_weather_data

    def parse_JSON_weather_data(self, weather_data):
        """ Gets the JSON output from the URL and parses it"""
        try:
            self.parsed_json_weather = json.loads(weather_data)
        except:
            print log_output().print_output("weather data is not in JSON format.")
            

    def forecast_issue_time(self):
        self.utc_issue_time = datetime.datetime.fromtimestamp(self.parsed_json_weather['currently']['time']).strftime('%H:%M')
        return  self.utc_issue_time
            
        # self.precipitation_probability = self.parsed_json_weather['currently']['precipProbability']
        # self.temperature_Min = self.parsed_json_weather['currently']['temperature']
        # self.temperature_Max = parsed_json_weather['currently']['temperatureMax']
        # self.summary = self.parsed_json_weather['currently']['summary']
        # TEST ITERATION THROUGH AN ARRAY OF DICTIONARIES


        #for key, value in self.parsed_json_weather['hourly']['data'].iteritems() :
         #   print key      
        
        # print self.precipitation_probability
        # print self.temperature_Min
        # print self.temperature_Max
        # print self.summary
        # print "Latitude: {0}\nLongtitude: {1}\nTimeZone: {2}".format(self.parsed_json['latitude'], self.parsed_json['longitude'], self.parsed_json['timezone'])
        # print len(self.test)
        
        
    def will_it_rain(self, time_frame):
        """ Scrapes whether it will rain today in the next n hours """
        self.hours = time_frame
        self.weather_type_array = []
        self.rain_status = []
        
        for hours in range(self.hours):
            # puts aweather data for each hour into 1 array
            self.weather_type_array.append(self.parsed_json_weather['hourly']['data'][hours]['icon'])
            # If rain in next n hours, 
            if 'rain' in self.parsed_json_weather['hourly']['data'][hours]['icon']:
                self.rain_time = self.parsed_json_weather['hourly']['data'][hours]['time']
                self.rain_time_utc = datetime.datetime.fromtimestamp(int(self.rain_time)).strftime('%H:%M')
                self.rain_probability = self.parsed_json_weather['hourly']['data'][hours]['precipProbability']
                self.rain_status.append("{1}% chance of rain at {0}.".format(self.rain_time_utc, self.rain_probability * 100))

        # If no rain data in array, return no rain expected.         
        if not 'rain' in self.weather_type_array:
            self.rain_status.append("No rain is expected in the next {0} hours.".format(self.hours))
            
        # print log_output().print_output("\n".join(self.rain_status))
        
        return  log_output().print_output("\n".join(self.rain_status))


class BuildDarkskyURL(object):
    """ Class to build Source URL. """
    def __init__(self, coordinates):
        self.string_coord = coordinates
        self.darksky_URL = "https://api.darksky.net/forecast"
        self.unique_key = "f5ec084a7185b6faf1753a7b8ff13a39"


    def build_url(self):
        self.data_url = "{0}/{1}/{2}".format(self.darksky_URL, self.unique_key, self.string_coord)
        return self.data_url


class GetLocation(object):
    ''' Class which will data scrape a json file with current location information'''

    def __init__(self):
        self.get_user_location = ""


    def get_literal_location(self):
        """ Gets your current location and returns the value in a string literal i.e "Cardiff" """

        try:
            print log_output().print_output("Atempting to get your location...")
            self.get_user_location = urllib2.urlopen('http://freegeoip.net/json/')
            self.user_location_json = self.get_user_location.read()
            self.get_user_location.close()
        except:
            # Defaults to a JSON structure of Cardiff if location attempt fails.
            print log_output().print_output("Unable to get your location, defaulting to Cardiff...")
            self.user_location_json = '{"city": "Cardiff", "region_code": "WLS", "re_name": "United Kingdom", "zip_code": "CF14"}'

        # print self.user_location_json
        self.user_location_dict = json.loads(self.user_location_json)
        self.user_location = self.user_location_dict['city']
        self.user_location_out = "... Your location is {0}".format(self.user_location)
        # print log_output().print_output(self.user_location_out)

        return self.user_location


    def get_coordinates(self, location_name):
        """ Gets the coordinates of the string literal location """
        geolocator = Nominatim()
        self.geolocator_output = geolocator.geocode(location_name)
        self.location_latitude = self.geolocator_output.latitude
        self.location_longitude = self.geolocator_output.longitude
        self.coordinate_string = "{0},{1}".format(self.location_latitude, self.location_longitude)
        return self.coordinate_string


class log_output(object):
    """ Class to log all program output to a text file. Rafactor with 
        logging API when I have time https://pymotw.com/2/logging/"""

    def __init__(self):
        self.input_string = ""


    def print_output(self, string):
        self.input_string = string
        self.log_output(self.input_string)
        return self.input_string

        
    def log_output(self, log):
        self.log_time = time.strftime("%c")
        self.log_to_file = "\n{0}: \n{1}".format(self.log_time, log) 
        with open('output_log.txt','a') as out_file:
            out_file.write(self.log_to_file)

            
if __name__ == "__main__":
    ins = GetLocation()
    location = ins.get_literal_location()
    lat_long = ins.get_coordinates(location)
    #print lat_long
    url = BuildDarkskyURL(lat_long).build_url()
    #print url
    weather = GetWeather()
    weather_data = weather.get_weather_data(url)
    # print weather_data
    weather.parse_JSON_weather_data(weather_data)
    weather.will_it_rain()
